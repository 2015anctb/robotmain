#include "RasPiMS/RasPiMS.hpp"
#include "RasPiDS3/RasPiDS3.hpp"
#include <fstream>
#include <iostream>
#include <math.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <wiringPi.h>
#include <thread>

using namespace RPMS;
using namespace RPDS3;
using namespace std;

char* SettingFileName = "MotorSetting";

string pathGet();
const int NumMotors = 8;

ofstream Log;

const int BeltSwitchPin = 23;

const short BeltSpeed = 180;
const int BeltRunTime = 1500; // (ms)

const short defaultRollerPower = 162;
const int RollerChangeAmount = 1;

const int PitchRunTime = 1500; // (ms)

Motor *beltPointer;

void beltStop(void) {
	while (digitalRead(BeltSwitchPin));
	beltPointer->spin(0);
}


int main(void) {
	cout << "Start" << endl;
	// プログラム起動時刻の取得
	time_t progStartTime = time(NULL);
	struct tm *pnow = localtime(&progStartTime);
	// ログ・ファイルを開く
	string path(pathGet().c_str());
	path.erase(path.find_last_of('/'));
	Log.open(path + "/Log.txt");
	Log << pnow->tm_year + 1900<< ":" << pnow->tm_mon + 1 << ":" << pnow->tm_mday << ":" <<  pnow->tm_hour << ":" << pnow->tm_min << ":" << pnow->tm_sec << endl;
	Log << "Path :" + path << endl;
	// コントローラーの接続・接続チェック
	DualShock3 controller(false, 0);
	if (!controller.connectedCheck()) {
		cout << "Couldn't connect." << endl;
		Log << "Couldn't connect to controller." << endl;
		return -1;
	}
	Log << "Success connect to controller." << endl;
	// モータードライバドライバとの通信のセットアップ
	MotorSerial ms;
	try {
		ms.init();
	}
	catch(runtime_error exception) {
		cerr << "Failed setup MotorSerial." << endl;
		Log << "Failed setup MotorSerial. runtime_error" << endl;
		return -1;
	}
	catch(...) {
		cerr << "Error" << endl;
		Log << "Failed setup MotorSerial." << endl;
		return 1;
	}
	Log << "Success setup MotorSerial." << endl;
	// モーターの設定の読み込み, 反映
	MotorDataFormat MotorDatas[NumMotors];
	if (loadMotorSetting(SettingFileName, MotorDatas, NumMotors)) {
		cerr << "MotorSetting load failed." << endl;
		Log << "MotorSetting load failed." << endl;
		return -1;
	}
	for (int i = 0; i < NumMotors; ++i) {
		Log << (int)(MotorDatas[i].id) << " " << (int)(MotorDatas[i].mNum) << " " << (double)(MotorDatas[i].magni) << endl;
	}

	Motor rightFront(MotorDatas[0], &ms);
	Motor leftFront(MotorDatas[1], &ms);
	Motor rightBack(MotorDatas[2], &ms);
	Motor leftBack(MotorDatas[3], &ms);
	Motor rightRoller(MotorDatas[4], &ms);
	Motor leftRoller(MotorDatas[5], &ms);
	Motor belt(MotorDatas[6], &ms);
	Motor pitch(MotorDatas[7], &ms);

	cout << "Standby MotorDriverDriver Communication." << endl;
	Log << "Standby MotorDriverDriver Communication." << endl;

	Log << "Belt Run." << endl;
	pinMode(BeltSwitchPin, INPUT);
	pullUpDnControl(BeltSwitchPin, PUD_UP);
	beltPointer = &belt;
	belt.spin(BeltSpeed);
	thread beltThread = thread([&]{ beltStop(); });

	Log << "MainLoop Start." << endl;
	// この下にロボットの処理を書く
	UPDATELOOP (controller, !controller.button(START)) {	// STARTボタンが押されるまで無限ループ
		if (controller.press(SELECT)) {
			Log << "Pause" << endl;
			ms.send(255, 255, 0);
			UPDATELOOP (controller, !controller.press(SELECT) and !controller.button(START));
			Log << "Pause release." << endl;
		}
		// ここから足回り
		// コントローラのスティックからロボット自体のベクトルを決定する
		short x = (short)(controller.stick(LEFT_X) + controller.stick(RIGHT_X));
		short y = (short)(controller.stick(LEFT_Y) + controller.stick(RIGHT_Y));

		// 足回りのモーターを動かす
		// 旋回
		int spinPower = 0;
		if (controller.button(L2)) {
			// 左回りの処理を書く
			spinPower += controller.stick(LEFT_T);
		}
		if (controller.button(R2)) {
			// 右回りの処理を書く
			spinPower += -1 * controller.stick(RIGHT_T);
		}
		spinPower = (int)(spinPower / 3.0);
		if (controller.button(LEFT)) {
			spinPower = 10;
		}
		if (controller.button(RIGHT)) {
			spinPower = -10;
		}
		// コントローラからのベクトルをロボットを動かしたいベクトルに変換
		short lfrbPower = (short)(-1.41421356 * (x + y));
		short rflbPower = (short)(1.41421356 * (x - y));
		double moveMagni = 0.8;
		rightFront.spin(moveMagni * (rflbPower + spinPower));	// 右前
		leftFront.spin(moveMagni * (lfrbPower - spinPower));	// 左前
		rightBack.spin(moveMagni * (lfrbPower + spinPower));	// 右後
		leftBack.spin(moveMagni * (rflbPower - spinPower));	// 左後
		// ここからローラー関連
		static short rollerPower = defaultRollerPower; // ローラーの回転数
		static bool rollerSpin = false; // ローラーを回すかどうか
		if (controller.press(TRIANGLE)) {
			// ローラーのonoff
			rollerSpin = !rollerSpin;
		}
		// ローラーの回転数を変更
		if (controller.button(L1) && controller.button(R1)) {
			rollerPower = defaultRollerPower;
			cout << (int)rollerPower << endl;
			Log << (int)rollerPower << endl;
		} else {
			if (controller.press(L1)) {
				rollerPower += RollerChangeAmount;
				cout << (int)rollerPower << endl;
				Log << (int)rollerPower << endl;
			}
			if (controller.press(R1)) {
				rollerPower -= RollerChangeAmount;
				cout << (int)rollerPower << endl;
				Log << (int)rollerPower << endl;
			}
		}
		if (rollerSpin) {
			// ローラーを回す
			rightRoller.spin(rollerPower);
			leftRoller.spin(rollerPower);
		} else {
			rightRoller.spin(0);
			leftRoller.spin(0);
		}

		// Belt
		if (controller.press(CIRCLE)) {
			belt.spin(BeltSpeed);
		}
		if (controller.release(CIRCLE)) {
			belt.spin(0);
		}

		if (controller.press(CROSS)) {
			ms.send(18, 3, 0);
		}
		if (controller.release(CROSS)) {
			ms.send(18, 2, 0);
		}

		// Pitching Machine
		// ボタンを押している間ピッチングマシーンを回し続ける
		if (controller.press(SQUARE)) {
			pitch.spin(MaxMotorPower);
		}
		if (controller.release(SQUARE)) {
			pitch.spin(0);
		}
	}

	if (beltThread.joinable())
		beltThread.join();

	Log << "Successful completion" << endl;
	return 0;
}

string pathGet() {
	// 現在のパスを取得
	char buf[512] = {};
	readlink("/proc/self/exe", buf, sizeof(buf) - 1); 	// 実行ファイルのパスを取得
	string path(buf);
	return path;
}

